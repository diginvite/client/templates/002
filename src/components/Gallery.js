import React, { useEffect, useState } from "react";
import Gallery from "react-grid-gallery";

const Gallery2 = (props) => {
	const [images, setImages] = useState([]);

	useEffect(() => {
		if (props.data) {
			// eslint-disable-next-line
			const newImages = props.data.detail.images.map((image) => {
				if (image.isGallery) {
					return {
						src: image.url,
						thumbnail: image.url,
						// thumbnailWidth: 320,
						// thumbnailHeight: 174,
						// isSelected: true,
						caption: image.description,
					};
				}
			});
			setImages(newImages);
		}
	}, [props]);

	return (
		<div id="fh5co-gallery" className="fh5co-section-gray">
			<div className="container">
				<div className="row">
					<div className="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
						<span>Our Memories</span>
						<h2>Wedding Gallery</h2>
						{/* <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> */}
					</div>
				</div>
				<div className="row row-bottom-padded-md">
					<div className="col-md-12">
						<Gallery images={images} />
					</div>
				</div>
			</div>
		</div>
	);
};

export default Gallery2;
