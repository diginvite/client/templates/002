import React from "react";
import Moment from "react-moment";
// import { Map, GoogleApiWrapper, Marker } from "google-maps-react";

const Event = (props) => {
  return (
    <div
      id="fh5co-event"
      className="fh5co-bg"
      // style="background-image:url(images/img_bg_3.jpg);"
    >
      <div className="overlay"></div>
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
            <span>Our Special Events</span>
            <h2>Wedding Events</h2>
          </div>
        </div>
        <div className="row">
          <div className="display-t">
            <div className="display-tc">
              <div className="col-md-10 col-md-offset-1">
                {props.data &&
                  props.data.detail.events.map((data, i) => {
                    return (
                      <div className="col-md-6 col-sm-6 text-center" key={i}>
                        <div className="event-wrap">
                          <h3>{data.name}</h3>
                          <div className="event-col">
                            <i className="icon-clock"></i>
                            <span>
                              <Moment format="HH:mm A">{data.startDate}</Moment>
                            </span>
                            <span>
                              <Moment format="HH:mm A">{data.endDate}</Moment>
                            </span>
                          </div>
                          <div className="event-col">
                            <i className="icon-calendar"></i>
                            {/* <span>Monday 28</span> */}
                            <span>
                              <Moment format="dddd DD">{data.startDate}</Moment>
                            </span>
                            <span>
                              <Moment format="MMMM, YYYY">
                                {data.startDate}
                              </Moment>
                            </span>
                          </div>
                          <p>
                            {data.address}
                            <br />
                            <small>"{data.note}"</small>
                          </p>
                          <center>
                            <a
                              href={`http://maps.google.com/maps?z=12&t=m&q=loc:${data.lat}+${data.lng}`}
                              target="blank"
                            >
                              <i className="icon-map"> View maps</i>
                            </a>
                          </center>
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Event;
