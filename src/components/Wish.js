import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Wish = (props) => {
  const [wishes, setWishes] = useState([]);

  useEffect(() => {
    if (props.data) {
      const newWishes = props.data.detail.wishes.map((data, i) => {
        return (
          <div className="item" key={i}>
            <div className="testimony-slide text-center">
              <span>{data.name}</span>
              <blockquote>
                <p>"{data.wish}"</p>
              </blockquote>
            </div>
          </div>
        );
      });
      setWishes(newWishes);
    }
  }, [props]);
  const settings = {
    dots: true,
    infinite: true,
    // speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 3000,
    autoplaySpeed: 2000,
  };
  return (
    <div id="fh5co-testimonial">
      <div className="container">
        <div className="row">
          <div className="row animate-box">
            <div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
              <span>Best Wishes</span>
              <h2>Friends Wishes</h2>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 animate-box">
              <div className="wrap-testimony">
                {/* <div className="owl-carousel-fullwidth"> */}
                <Slider {...settings}>{wishes}</Slider>
                {/* </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Wish;
