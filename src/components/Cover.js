import React, { useEffect, useState } from "react";
import Countdown from "react-countdown-now";

const renderer = ({ days, hours, minutes, seconds }) => {
  return (
    <>
      <div className="simply-section simply-days-section">
        <div>
          <span className="simply-amount">{days}</span>
          <span className="simply-word">DAYS</span>
        </div>
      </div>
      <div className="simply-section simply-days-section">
        <div>
          <span className="simply-amount">{hours}</span>
          <span className="simply-word">HOURS</span>
        </div>
      </div>
      <div className="simply-section simply-days-section">
        <div>
          <span className="simply-amount">{minutes}</span>
          <span className="simply-word">MINUTES</span>
        </div>
      </div>
      <div className="simply-section simply-days-section">
        <div>
          <span className="simply-amount">{seconds}</span>
          <span className="simply-word">SECONDS</span>
        </div>
      </div>
    </>
  );
};

const Cover = (props) => {
  const [backgroundImage, setBackgroundImage] = useState(null);

  useEffect(() => {
    if (props.data) {
      const newBackground = props.data.detail.images.filter(
        (image) => image.isCover === true
      )[0];
      setBackgroundImage(newBackground);
    }
  }, [props]);

  return (
    <header
      id="fh5co-header"
      className="fh5co-cover"
      role="banner"
      style={{
        backgroundImage: `url(${backgroundImage && backgroundImage.url})`,
      }}
      data-stellar-background-ratio="0.5"
    >
      <div className="overlay"></div>
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-md-offset-2 text-center">
            <div className="display-t">
              <div
                className="display-tc animate-box"
                data-animate-effect="fadeIn"
              >
                <h1>
                  {props.data &&
                    props.data.detail.couples.map((data, i) => {
                      return (
                        <React.Fragment key={i}>
                          {data.nickName} {i === 0 ? <>&amp; </> : null}
                        </React.Fragment>
                      );
                    })}
                </h1>
                <h2>We Are Getting Married</h2>
                <div className="simply-countdown simply-countdown-one">
                  <Countdown
                    date={props.data && props.data.detail.events[0].startDate}
                    renderer={renderer}
                  />
                </div>
                <p>
                  <a
                    href="#home"
                    className="btn btn-default btn-sm"
                    onClick={() => props.onClick("calendar")}
                  >
                    Save the date
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Cover;
