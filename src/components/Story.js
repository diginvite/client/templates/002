import React from "react";
import Moment from "react-moment";

const Story = (props) => {
  return (
    <div id="fh5co-couple-story">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
            <span>We Love Each Other</span>
            <h2>Our Story</h2>
            {/* <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p> */}
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 col-md-offset-0">
            <ul className="timeline animate-box">
              {props.data &&
                props.data.detail.stories.map((data, i) => {
                  var className = "";
                  if (i % 2 !== 0) {
                    className = "timeline-inverted";
                  }
                  return (
                    <li className={className} key={i}>
                      {/* <div className="timeline-badge" style="background-image:url(images/couple-1.jpg);"></div> */}
                      <div className="timeline-panel">
                        <div className="timeline-heading">
                          <h3 className="timeline-title">{data.title}</h3>
                          <span className="date">
                            <Moment format="MMMM DD, YYYY">{data.date}</Moment>
                          </span>
                        </div>
                        <div className="timeline-body">
                          <p>{data.description}</p>
                        </div>
                      </div>
                    </li>
                  );
                })}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Story;
