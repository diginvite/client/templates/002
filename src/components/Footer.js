import React from "react";

const Footer = () => {
  return (
    <footer id="fh5co-footer" role="contentinfo">
      <div className="container">
        <div className="row copyright">
          <div className="col-md-12 text-center">
            <p>
              <small className="block">
                &copy; 2019{" "}
                <a href="http://diginvite.com/" target="blank">
                  DIGINVITE
                </a>{" "}
                . All Rights Reserved.
              </small>
              {/* <small className="block">Designed by <a href="http://freehtml5.co/" target="_blank">FREEHTML5.co</a> Demo Images: <a href="http://unsplash.co/" target="_blank">Unsplash</a></small> */}
            </p>
            {/* <p> */}
            <ul className="fh5co-social-icons">
              <li>
                <a href="https://twitter.com/diginvite_" target="blank">
                  <i className="icon-twitter"></i>
                </a>
              </li>
              <li>
                <a href="https://instagram.com/diginvite_" target="blank">
                  <i className="icon-instagram"></i>
                </a>
              </li>
              {/* <li><a href="#"><i className="icon-facebook"></i></a></li> */}
              {/* <li><a href="#"><i className="icon-linkedin"></i></a></li> */}
              {/* <li><a href="#"><i className="icon-dribbble"></i></a></li> */}
            </ul>
            {/* </p> */}
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
