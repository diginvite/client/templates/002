import React from "react";
import Moment from "react-moment";

const Couple = (props) => {
  return (
    <div id="fh5co-couple">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
            <h2>Hello {props.invite && props.invite.name}</h2>
            <h3>
              <Moment format="MMMM DD, YYYY">
                {props.data && props.data.detail.events[0].startDate}
              </Moment>
            </h3>
            <p>We invited you to celebrate our wedding</p>
          </div>
        </div>
        <div className="couple-wrap animate-box">
          {props.data &&
            props.data.detail.couples.map((data, i) => {
              return (
                <React.Fragment key={i}>
                  <div className="couple-half">
                    <div className={i === 0 ? "groom" : "bride"}>
                      <img
                        src={data.images[0].url}
                        alt="groom"
                        className="img-responsive"
                      />
                    </div>
                    <div className={i === 0 ? "desc-groom" : "desc-bride"}>
                      <h3>{data.name}</h3>
                      <p>{data.description}</p>
                    </div>
                  </div>
                </React.Fragment>
              );
            })}
          <p className="heart text-center">
            <i className="icon-heart2"></i>
          </p>
          {/* <div className="couple-half">
            <div className="bride">
              <img src="images/bride.jpg" alt="groom" className="img-responsive"/>
            </div>
            <div className="desc-bride">
              <h3>Sheila Mahusay</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove</p>
            </div>
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default Couple;
