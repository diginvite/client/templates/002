import React from "react";

const Guest = (props) => {
	return (
		<div
			id="fh5co-started"
			className="fh5co-bg"
			s
			// tyle="background-image:url(images/img_bg_4.jpg);"
		>
			<div className="overlay"></div>
			<div className="container">
				<div className="row animate-box">
					<div className="col-md-8 col-md-offset-2 text-center fh5co-heading">
						<h2>Are You Attending?</h2>
						<p>
							Please Fill-up the form to notify you that you're attending. Thanks.
						</p>
					</div>
				</div>
				<div className="row animate-box">
					<div className="col-md-10 col-md-offset-1">
						<form className="form-inline" onSubmit={props.onSubmit}>
							<div className="col-md-6 col-sm-6">
								<div className="form-group">
									<label for="name" className="sr-only">
										Name
									</label>
									<input
										type="text"
										className="form-control"
										id="name"
										placeholder="Name"
										name="name"
										value={props.name}
										onChange={(e) => props.onChange(e)}
									/>
								</div>
							</div>
							<div className="col-md-6 col-sm-6">
								<div className="form-group">
									<label for="company" className="sr-only">
										From
									</label>
									<input
										type="text"
										className="form-control"
										id="company"
										placeholder="From"
										name="company"
										value={props.company}
										onChange={(e) => props.onChange(e)}
									/>
								</div>
							</div>
							<div className="col-md-6 col-sm-6">
								<div className="form-group">
									<label for="email" className="sr-only">
										Attend
									</label>
									<select
										className="form-control"
										name="attend"
										value={props.attend}
										onChange={(e) => props.onChange(e)}
									>
										<option value="1">Attending</option>
										<option value="2">Tentative</option>
										<option value="0">Not Attending</option>
									</select>
								</div>
							</div>
							<div className="col-md-6 col-sm-6">
								<div className="form-group">
									<label for="person" className="sr-only">
										Person
									</label>
									<select
										className="form-control"
										name="person"
										value={props.person}
										onChange={(e) => props.onChange(e)}
									>
										<option value="1">1 Person</option>
										<option value="2">2 Persons</option>
									</select>
								</div>
							</div>
							<div className="col-md-12 col-sm-12">
								<div className="form-group">
									<label for="wish" className="sr-only">
										Wish
									</label>
									<textarea
										className="form-control"
										placeholder="Wish"
										rows="5"
										name="wish"
										value={props.wish}
										onChange={(e) => props.onChange(e)}
									></textarea>
								</div>
							</div>
							<div className="col-md-12 col-sm-12">
								<button type="submit" className="btn btn-default btn-block">
									Save
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Guest;
