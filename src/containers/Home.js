import React, { Component } from "react";
import axios from "axios";
import moment from "moment";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { Helmet } from "react-helmet";
// import { Button, Image, Modal, Icon } from 'semantic-ui-react'
// import 'semantic-ui-css/semantic.min.css'
import "../App.css";

import Header from "../components/Header";
import Cover from "../components/Cover";
import Couple from "../components/Couple";
import Event from "../components/Event";
import Story from "../components/Story";
import Gallery from "../components/Gallery";
import Wish from "../components/Wish";
import Guest from "../components/Guest";
import Footer from "../components/Footer";
import Modal from "../components/Modal";

// import { apiUrl } from "../actions/config";

class Home extends Component {
	constructor() {
		super();
		this.state = {
			order: null,
			invite: null,
			name: "",
			company: "",
			attend: "1",
			person: 1,
			wish: "",
			modal: false,
			music: false,
		};
	}

	componentDidMount = () => {
		var url_string = document.URL;
		var url = new URL(url_string);
		var hostname = url.hostname;
		var hostnames = hostname.split(".");
		var param = url.searchParams.get("invite");

		axios
			.get(
				`${process.env.REACT_APP_API_URL}/findOrder?domain=${hostnames[0]}&invitation=${param}`
			)
			.then((response) => {
				this.setState({
					order: response.data.order,
					invite: response.data.invitation,
				});
			})
			.catch((error) => {
				throw error;
			});
	};

	onAction = (flag) => {
		if (flag === "calendar") {
			var startDate = moment(this.state.order.detail.events[0].startDate).format(
				"YYYYMMDD"
			);
			var startTime = moment(this.state.order.detail.events[0].startDate).format(
				"HHmmss"
			);
			var endDate = moment(this.state.order.detail.events[0].endDate).format(
				"YYYYMMDD"
			);
			var endTime = moment(this.state.order.detail.events[0].endDate).format(
				"HHmmss"
			);
			var location = this.state.order.detail.events[0].address;

			// var event = `https://calendar.google.com/calendar/r/eventedit?text=${this.state.order.name}&dates=${startDate}T110000/${endDate}T200000&ctz=Asia/Jakarta;`
			var url = `https://calendar.google.com/calendar/r/eventedit?text=${this.state.order.name}&dates=${startDate}T${startTime}/${endDate}T${endTime}&ctz=Asia/Jakarta&details=Kami akan sangat gembira ketika kita bisa bertemu di acara kami.<br><br>salam hangat&location=${location}&pli=1&amp;uid=1521339627addtocalendar&sf=true&amp;output=xml`;
			var win = window.open(url, "_blank");
			win.focus();
		}
	};

	onChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

	onSubmit = (e) => {
		e.preventDefault();
		if (this.state.name === "") {
			toast.error("Name cannt be null", {
				position: toast.POSITION.TOP_RIGHT,
			});
		} else {
			if (this.state.attend === "") {
				toast.error("Attend cannt be null", {
					position: toast.POSITION.TOP_RIGHT,
				});
			} else {
				const data = {
					name: this.state.name,
					company: this.state.company,
					attend: this.state.attend,
					qty: this.state.person,
					wish: this.state.wish,
				};

				axios
					.post(
						`${process.env.REACT_APP_API_URL}/insertOneWish/${this.state.order.id}`,
						data
					)
					.then((resp) => {
						this.setState({
							order: resp.data,
							name: "",
							company: "",
							wish: "",
						});
						toast.success("Data saved", {
							position: toast.POSITION.TOP_RIGHT,
						});
					})
					.catch((error) => {
						throw error;
					});
			}
		}
	};

	onTogglePlay(param) {
		var x = document.getElementById("myAudio");
		if (param === "play") {
			x.play();
		} else {
			x.pause();
		}
		this.setState({ music: !this.state.music });
	}

	render() {
		return (
			<>
				<div className="application">
					<Helmet>
						<meta charSet="utf-8" />
						<title>{this.state.order && this.state.order.name}</title>
					</Helmet>
				</div>
				<div className="fh5co-loader"></div>
				<div id="page">
					<Header />
					<Cover data={this.state.order} onClick={(flag) => this.onAction(flag)} />
					<Couple data={this.state.order} invite={this.state.invite} />
					<Event data={this.state.order} />
					<Story data={this.state.order} />
					<Gallery data={this.state.order} />
					<Wish data={this.state.order} />
					<Guest
						data={this.state.order}
						name={this.state.name}
						company={this.state.company}
						attend={this.state.attend}
						person={this.state.person}
						wish={this.state.wish}
						onChange={(e) => this.onChange(e)}
						onSubmit={(e) => this.onSubmit(e)}
					/>
					<Footer />
					{this.state.invite ? (
						<Modal
							visible={this.state.modal}
							invite={this.state.invite}
							onClose={() => {
								this.setState({ modal: false });
							}}
						/>
					) : null}
				</div>
				<ToastContainer />
				<audio
					loop=""
					preload="auto"
					id="myAudio"
					src={this.state.order && this.state.order.detail.songs[0].url}
				/>
				{!this.state.music ? (
					<a
						href="#home"
						className="music"
						onClick={() => this.onTogglePlay("play")}
					>
						<i className="icon-music"></i>
					</a>
				) : (
					<a
						href="#home"
						className="music"
						onClick={() => this.onTogglePlay("pause")}
					>
						<i className="icon-volume"></i>
					</a>
				)}
			</>
		);
	}
}

export default Home;
